
window.onload = function(){
    var inputSearch = document.getElementById("input-search");
    var CardContainer = document.getElementById("card-container");
    var searchBtn = document.getElementById("search-btn");
    inputSearch.addEventListener("keyup",function(event){
        var search = document.getElementById(event.srcElement.id).value;
        if(search.length > 2){
            main.suggestions(search);
        }
    });

    searchBtn.addEventListener("click",()=>{

        if(inputSearch.value != ""){
            main.createMovieCards(inputSearch.value);
        }else{
            main.displayMsg();   
        }
    });    
}
