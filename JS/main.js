    let main = (()=>{
    //let omdbapi = "Creating first Module";
    let omdbapi = "https://www.omdbapi.com/?apikey=c6aad4b8&" 

    function getMovies(movies){
        movies.Search.map((movie)=>{
            console.log(movie);
        });    
    }

    //Displays a message of a error while searching
    function displayMsg(){
        let msgArea = document.getElementById("msg-area")
        if (msgArea.style.display !== 'none') {
            msgArea.style.display = 'none';
        }
        else {
            msgArea.style.display = 'block';
        }
    }

    function displaySuggestions(movies){

        let suggestions = document.getElementById("suggestions");
        movies.Search.map((movie)=>{

            

        } )
    }



    //Displays the search 
    function displayMovies(movies){
        
        let cardContainer = document.getElementById("movie-container");
        
        cardContainer.innerHTML="";
        movies.Search.map((movie)=>{
            
            let movieContainer = document.createElement("div");
            let title = document.createElement("p");
            let poster = document.createElement("img");
            let year = document.createElement("p");

            movieContainer.classList.add("movie");
            poster.classList.add("movie-img");
            title.classList.add("card-title");
            year.classList.add("card-text");

            title.innerText = movie.Title;
            poster.src = movie.Poster == "N/A" ? "https://www.vuecinemas.nl/img/movie_placeholder.png": movie.Poster
            year.innerHTML = movie.Year;
            
            movieContainer.appendChild(poster);
            movieContainer.appendChild(title);
            movieContainer.appendChild(year);
            cardContainer.appendChild(movieContainer);
        });
    }

    //handles the request of movies by search 
    function setMoviesHandle(fn){
        return function fetchMovies(text){
            fetch(omdbapi + "s=" + text)
                .then((response)=> response.json()) // converts the return in JSON
                .then(fn)
                .catch((error)=>{
                    displayMsg();
                });
        }
    }

    let suggestions = setMoviesHandle(getMovies); //create a function get suggestions
    let createMovieCards = setMoviesHandle(displayMovies); // create function to
    let test = setMoviesHandle(getMovies);

    return{
        suggestions:suggestions,
        createMovieCards:createMovieCards,
        test:test,
        displayMsg:displayMsg
    }

})();
