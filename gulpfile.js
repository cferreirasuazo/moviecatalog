var gulp = require('gulp');
var sass = require("gulp-sass");
var browserSync = require("browser-sync").create();

gulp.task("serve",()=>{
        browserSync.init({
            server:'./'
        });
  //      gulp.src("sass/**/*.scss").pipe(sass()).pipe(gulp.dest("dist"));
        gulp.watch("*.html").on("change",browserSync.reload);
//        gulp.watch("sass/**/*.scss").on("change",browserSync.reload);
        gulp.watch("dist/*.css").on("change",browserSync.reload);
        gulp.watch("JS/*.js").on("change",browserSync.reload);
})


//gulp.task('default',['serve','build-sass']);
